package com.raywenderlich.eevargass.mylistmaker.service

import com.google.gson.annotations.SerializedName

data class User(
    @SerializedName("id") var id: Int? = null,
    @SerializedName("name") val name: String? = null,
    @SerializedName("username") var userName: String? = null,
    @SerializedName("email") val email: String? = null,
    @SerializedName("phone") val phone: String? = null,
    @SerializedName("website") val website: String? = null
)
