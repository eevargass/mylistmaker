package com.raywenderlich.eevargass.mylistmaker.service

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ApiClient {

    companion object Singleton{
        private val BASE_URL = "https://jsonplaceholder.typicode.com/"
        private var retrofit: Retrofit? = null

        fun getApiClient(): JsonPlaceHolderApi {
            if (retrofit == null) {
                retrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
            }
            return retrofit!!.create(JsonPlaceHolderApi::class.java)
        }
    }
}