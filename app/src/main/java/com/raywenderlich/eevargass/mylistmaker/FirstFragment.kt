package com.raywenderlich.eevargass.mylistmaker

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.raywenderlich.eevargass.mylistmaker.recyclerview.MyListAdapter
import com.raywenderlich.eevargass.mylistmaker.service.ApiClient
import com.raywenderlich.eevargass.mylistmaker.service.JsonPlaceHolderApi
import com.raywenderlich.eevargass.mylistmaker.service.User
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.Call

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class FirstFragment : Fragment() {

    lateinit var todoListRecyclerView: RecyclerView

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_first, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        todoListRecyclerView=view.findViewById<RecyclerView>(R.id.lists_recyclerView)
        //todoListRecyclerView.layoutManager = LinearLayoutManager(view.context)
        todoListRecyclerView.adapter =
            MyListAdapter()

        GlobalScope.launch(context = Dispatchers.IO) {
            loadResultsAsync()
        }

        //view.findViewById<Button>(R.id.button_first).setOnClickListener {
        //    findNavController().navigate(R.id.action_FirstFragment_to_SecondFragment)
        //}
    }

    //meter corutina aca!!!
    private fun loadResultsAsync() {
        //Al parecer no debo ejecutar llamadas a servicios en el main thread. Código para habilitar la llamada
        //val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        //StrictMode.setThreadPolicy(policy)
        //Preguntar ¿por que necesito permiso de internet en el manifest?

        val jsonPlaceHolder: JsonPlaceHolderApi = ApiClient.getApiClient()
        val myCall: Call<List<User>> = jsonPlaceHolder.getUsers()
        val result = myCall.execute().body()!!
        GlobalScope.launch(context = Dispatchers.Main) {//Si no hago esto me da exception de que solo el  "original thread" puede modificar las vistas
            (todoListRecyclerView.adapter as MyListAdapter).setData(result)
        }
        printOnConsole(result)
        return
    }

    private fun printOnConsole(result: List<User>) {
        Log.d(
            "CARGA_DATA",
            "----------------------------------------------------------------------------------------"
        )
        for (item in result) {
            Log.d(
                "CARGA_DATA",
                "id: " + item.id + ", name:" + item.name + ", item.userName:" + item.userName + ", item.email:" + item.email
            )
        }
        Log.d(
            "CARGA_DATA",
            "----------------------------------------------------------------------------------------"
        )
    }
}