package com.raywenderlich.eevargass.mylistmaker.service

import retrofit2.Call
import retrofit2.http.GET

interface JsonPlaceHolderApi {

    @GET("users")
    fun getUsers(): Call<List<User>>

    @GET("posts")
    fun getPosts(): Call<List<Post>>
}