package com.raywenderlich.eevargass.mylistmaker.recyclerview

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.DialogFragmentNavigatorDestinationBuilder
import androidx.recyclerview.widget.RecyclerView
import com.raywenderlich.eevargass.mylistmaker.R
import com.raywenderlich.eevargass.mylistmaker.service.User

class MyListAdapter: RecyclerView.Adapter<RecyclerView.ViewHolder>()  {
    //private val todoList = arrayOf("Android development", "House Work", "Errands", "Shopping", "studying", "cooking")
    private var usersList = listOf<User>()
    private val WHITE_POSITION = 0
    private val DARK_POSITION = 1

    private lateinit var parent: ViewGroup
    //Usar sealed class aqui. hacer traduccion a strings de cada objeto

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        this.parent = parent
        if (viewType == WHITE_POSITION) {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.white_list_view_holder, parent, false)
            return WhiteListViewHolder(view)
        }else { // case DARK_POSITION
            val view = LayoutInflater.from(parent.context).inflate(R.layout.dark_list_view_holder, parent, false)
            return DarkListViewHolder(view)
        }
        // return MyListViewHolder(view)  //Antes tenia un unico viewHolder
    }

    override fun getItemCount(): Int {
        return usersList.size
    }

    public fun setData(newList: List<User>){
        usersList = newList
        this.notifyDataSetChanged()
    }

    //Funcion para devolver el tipo de item
    override fun getItemViewType(position: Int): Int {
        return ( if(position % 2 == 0) WHITE_POSITION else DARK_POSITION)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        //holder.todoPositionTextView.text = (position + 1).toString()
        //holder.todoTextTextView.text = todoList[position]

        if (holder is WhiteListViewHolder) {
            holder.idTextView.text = usersList.get(position).id.toString()
            holder.nameTextView.text = usersList.get(position).name
            holder.phoneTextView.text = usersList.get(position).phone

            holder.emailTextView.text = usersList.get(position).email
        }else if (holder is DarkListViewHolder){
            holder.idTextView.text = usersList.get(position).id.toString()
            holder.nameTextView.text = usersList.get(position).name
            holder.phoneTextView.text = usersList.get(position).phone

            holder.userNameTextView.text = usersList.get(position).userName
        }else{
            Log.d("MyAppTAG", "onBindViewHolder: Holder no es ni de tipo WhiteListViewHolder ni DarkListViewHolder")
        }

        holder.itemView.setOnClickListener {
            Toast.makeText(this.parent.context, "website de la persona es => ${usersList.get(position).website}", Toast.LENGTH_LONG).show()
        }
    }
}
