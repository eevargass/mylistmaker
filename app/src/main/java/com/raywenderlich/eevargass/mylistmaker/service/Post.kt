package com.raywenderlich.eevargass.mylistmaker.service

import com.google.gson.annotations.SerializedName

data class Post(
    @SerializedName("id") var id: Int? = null,
    @SerializedName("userid") val userId: Int? = null,
    @SerializedName("title") val title: String? = null,
    @SerializedName("body") val text: String? = null
)
