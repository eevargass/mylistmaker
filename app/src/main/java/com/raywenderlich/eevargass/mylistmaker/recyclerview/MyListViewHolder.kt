package com.raywenderlich.eevargass.mylistmaker.recyclerview

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.raywenderlich.eevargass.mylistmaker.R
import kotlinx.android.synthetic.main.dark_list_view_holder.view.*
import kotlinx.android.synthetic.main.white_list_view_holder.view.*
import kotlinx.android.synthetic.main.white_list_view_holder.view.itemPhone

class WhiteListViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {
    var idTextView = itemView.findViewById<TextView>(R.id.itemId)
    var nameTextView = itemView.findViewById<TextView>(R.id.itemName)
    var emailTextView = itemView.itemEmail
    var phoneTextView = itemView.itemPhone
}

class DarkListViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {
    var idTextView = itemView.findViewById<TextView>(R.id.itemId)
    var nameTextView = itemView.findViewById<TextView>(R.id.itemName)
    var userNameTextView = itemView.itemUserName
    var phoneTextView = itemView.itemPhone
}